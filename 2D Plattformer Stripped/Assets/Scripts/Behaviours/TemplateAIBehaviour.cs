﻿using UnityEngine;
using System.Collections;
using System;

public class TemplateAIBehaviour : EnemyAI {


    // Wird alle stateUpdateFrequency Sekunden ausgeführt. CurrentState ist der aktuelle Zustand des Gegners.
    public override void StateUpdate(AIStates currentState)
    {
        //Ein "if" ist eine Bedingung. Der Code-Block unter dem "if" wird ausgeführt, wenn die Bedingung im "if" wahr ist.
        // == ist ein Vergleich. Dabei wird überprüft, ob der Wert links des "==" gleich dem Wert rechts davon ist.
        if (currentState == AIStates.ChasePlayer)
        {
            moveDestination = playerTransform;
        }
    }

    public override void ExecuteAction()
    {
        /* Hier kommt rein, was passieren soll, wenn der Aggro-Trigger des Gegners betreten wird */
        AIState = AIStates.ChasePlayer;

    }

}
