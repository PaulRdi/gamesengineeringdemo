﻿using UnityEngine;
using System.Collections;

public enum AIStates
{
    Idle = 0,
    ChasePlayer,
    Patrol
}

//[RequireComponent(typeof(AILerp))]
[RequireComponent(typeof(TriggerCheck))]
public abstract class EnemyAI : MonoBehaviour {


    AIStates startState = AIStates.Idle;


    private float stateUpdateFrequency;
    protected AIStates AIState
    {
        get { return _aistate; }
        set
        {
            if (stateRoutine != null)
                StopCoroutine(stateRoutine);

            stateRoutine = StartCoroutine(State_Update(value));
            _aistate = value;
        }
    }

    /// <summary>
    /// Eine Transform-Komponente einsetzen, um die Position der Transform-Komponente als Ziel zu setzen.
    /// </summary>
    protected Transform moveDestination
    {
        set
        {
            //lerper.target = value;
        }
    }
    protected Transform playerTransform
    {
        get
        {
            return the_player.transform;
        }
    }

    TriggerCheck the_collider;
    GameObject the_player;
    private AIStates _aistate;
    Coroutine stateRoutine;
    //AILerp lerper;


    void Awake ()
    {
        //lerper = GetComponent<AILerp>();
        the_player = GameObject.FindGameObjectWithTag("Player");
        stateUpdateFrequency = Time.fixedDeltaTime;
        the_collider = this.GetComponent<TriggerCheck>();
    }

    void Start ()
    {
        AIState = startState;
        the_collider.TriggerEntered += this.The_collider_TriggerEntered;
    }

    void OnDestroy()
    {
        the_collider.TriggerEntered -= this.The_collider_TriggerEntered;
    }

    private void The_collider_TriggerEntered(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
            ExecuteAction();
    }

    IEnumerator State_Update (AIStates state)
    {
        while (AIState == state)
        {
            StateUpdate(state);
            yield return new WaitForSeconds(stateUpdateFrequency);
        }
        stateRoutine = null;
    }

    public abstract void StateUpdate(AIStates currentState);
    public abstract void ExecuteAction();

}
