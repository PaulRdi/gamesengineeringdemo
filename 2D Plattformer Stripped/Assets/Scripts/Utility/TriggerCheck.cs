﻿using UnityEngine;
using System.Collections;

public delegate void TriggerDelegate(Collider2D other);

[RequireComponent(typeof(Collider2D))]
public class TriggerCheck : MonoBehaviour {

    public event TriggerDelegate TriggerEntered;

	void OnTriggerEnter2D(Collider2D other)
    {

        if (TriggerEntered != null)
            TriggerEntered(other);

    }

    void Start()
    {
        if (!this.GetComponent<Collider2D>().isTrigger)
            Debug.LogWarning("The Collider 2D is not a Trigger, set the Trigger-Flag in the Collider to true!");
    }
}
